<?php

namespace xtetis\ximg\models;

class SimpleImageModel
{

    /**
     * @var mixed
     */
    public $image;
    /**
     * @var mixed
     */
    public $image_type;

    /**
     * @param $filename
     */
    function load($filename)
    {

        $image_info       = getimagesize($filename);
        $this->image_type = $image_info[2];
        if (IMAGETYPE_JPEG == $this->image_type)
        {

            $this->image = imagecreatefromjpeg($filename);
        }
        elseif (IMAGETYPE_GIF == $this->image_type)
        {

            $this->image = imagecreatefromgif($filename);
        }
        elseif (IMAGETYPE_PNG == $this->image_type)
        {

            $this->image = imagecreatefrompng($filename);
        }
    }
    /**
     * @param $filename
     * @param $image_type
     * @param IMAGETYPE_JPEG $compression
     * @param $permissions
     */
    function save(
        $filename,
        $image_type = IMAGETYPE_JPEG,
        $compression = 75,
        $permissions = null
    )
    {

        if (IMAGETYPE_JPEG == $image_type)
        {
            imagejpeg($this->image, $filename, $compression);
        }
        elseif (IMAGETYPE_GIF == $image_type)
        {

            imagegif($this->image, $filename);
        }
        elseif (IMAGETYPE_PNG == $image_type)
        {

            imagepng($this->image, $filename);
        }
        if (null != $permissions)
        {

            chmod($filename, $permissions);
        }
    }
    /**
     * @param $image_type
     */
    function output($image_type = IMAGETYPE_JPEG)
    {

        if (IMAGETYPE_JPEG == $image_type)
        {
            imagejpeg($this->image);
        }
        elseif (IMAGETYPE_GIF == $image_type)
        {

            imagegif($this->image);
        }
        elseif (IMAGETYPE_PNG == $image_type)
        {

            imagepng($this->image);
        }
    }
    function getWidth()
    {

        return imagesx($this->image);
    }
    function getHeight()
    {

        return imagesy($this->image);
    }
    /**
     * @param $height
     */
    function resizeToHeight($height)
    {

        $ratio = $height / $this->getHeight();
        $width = floor($this->getWidth() * $ratio);
        $this->resize($width, $height);
    }

    /**
     * @param $width
     */
    function resizeToWidth($width)
    {
        $ratio  = $width / $this->getWidth();
        $height = floor($this->getheight() * $ratio);
        $this->resize($width, $height);
    }

    /**
     * @param $scale
     */
    function scale($scale)
    {
        $width  = $this->getWidth() * $scale / 100;
        $height = $this->getheight() * $scale / 100;
        $this->resize($width, $height);
    }


    /**
     * @param $width
     * @param $height
     */
    function resize(
        $width,
        $height
    )
    {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }





}

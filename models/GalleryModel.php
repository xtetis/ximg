<?php

namespace xtetis\ximg\models;

class GalleryModel extends \xtetis\xengine\models\TableModel
{
    /**
     * @var string
     */
    public $table_name = 'ximg_gallery';

    /**
     * ID заглавного изображения
     */
    public $id_main_img = 0;

    /**
     * ID родительской категории
     */
    public $id_parent = 0;

    /**
     * Имя галереи
     */
    public $name = '';

    /**
     * Дата создания записи
     */
    public $create_date = 0;

    /**
     * Результат получения данных из SQL запроса
     */
    public $result_sql = [];

    /**
     * Модель родительской галереи
     */
    public $parent_gallery_model = false;

    /**
     * Список дочерних моделей галерей
     */
    public $child_model_gallery_list = [];

    /**
     * Список ВСЕХ дочерних моделей галерей (рекурсивно)
     */
    public $all_child_model_gallery_list = [];

    /**
     * Список дочерних галерей
     */
    public $all_child_gallery_list_html = '';

    /**
     * Массив моделей изображений для указанной галереи
     */
    public $img_model_list = [];

    /**
     * ID категории галереи
     */
    public $id_category = 0;

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        parent::__construct($params);

        // Проверяет параметры
        \xtetis\ximg\Config::validateParams();
    }

    public function getById()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if (!$this->getModelById())
        {
            return false;
        }

        return true;
    }

    /**
     * Добавляем галерею
     */
    public function addGallery()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->name        = strval($this->name);
        $this->id_category = intval($this->id_category);

        if ($this->id_category)
        {
            $model_category = \xtetis\ximg\models\CategoryModel::generateModelById($this->id_category);
            if (!$model_category)
            {
                $this->addError('id_category', 'Указанная категория не существует');

                return false;
            }
        }

        if (!$this->id_category)
        {
            $this->id_category = null;
        }

        if (!strlen($this->name))
        {
            $this->addError('name', __FUNCTION__ . ': Не указано имя галереи');

            return false;
        }

        $this->name .= '_' . md5(uniqid());

        $this->insert_update_field_list = [
            'name',
            'id_category',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }

    /**
     * Возвращает список моделей дочерних
     */
    public function getChildModelList($recursive = true)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        // Возвращает список ID статей по родительской галлерее
        $this->result_sql['getGalleryIdByParent'] = \xtetis\ximg\models\SqlModel::getGalleryIdByParent(
            $this->id
        );

        if (!$this->result_sql['getGalleryIdByParent'])
        {
            $this->child_model_gallery_list = [];

            return $this->child_model_gallery_list;
        }

        $this->child_model_gallery_list = [];
        foreach ($this->result_sql['getGalleryIdByParent'] as $id)
        {
            $model_gallery = new self(
                [
                    'id' => $id,
                ]
            );

            $model_gallery->getById();
            if ($recursive)
            {
                $model_gallery->getChildModelList($recursive);
            }

            $this->child_model_gallery_list[$id]     = $model_gallery;
            $this->all_child_model_gallery_list[$id] = $model_gallery;

            if ($recursive)
            {
                $model_gallery->getChildModelList($recursive);
                foreach ($model_gallery->child_model_gallery_list as $id_child => &$model_child_gallery)
                {
                    foreach ($model_child_gallery as $id_subchild => &$model_subchild_gallery)
                    {
                        if (!isset($this->all_child_model_gallery_list[$id_subchild]))
                        {
                            $this->all_child_model_gallery_list[$id_subchild] = $model_subchild_gallery;
                        }
                    }
                }
            }
        }

        return $this->child_model_gallery_list;
    }

    /**
     * Возвращает список калерей в виде опций для select
     */
    public function getGalleryOptions(
        $level = 0,
        $exclude_id = 0
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->getChildModelList(true);

        $ret = [];
        foreach ($this->child_model_gallery_list as $id_child => $model_gallery)
        {
            if ($id_child != $exclude_id)
            {
                $ret[$id_child] = str_repeat('→', $level) . ' ' . $id_child . ' - ' . $model_gallery->name;

                foreach ($model_gallery->getGalleryOptions($level + 1, $exclude_id) as $key => $value)
                {
                    $ret[$key] = $value;
                }
            }

        }

        return $ret;
    }

    /**
     * Возвращает список калерей в виде списка ul li
     */
    public function getGalleryListHtml(
        $exclude_id = 0,
        $include_id = false
    )
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->getChildModelList(true);

        if ($this->child_model_gallery_list)
        {
            //echo "gallery_id=".$this->id."\n";
            $this->all_child_gallery_list_html = '<ul>';
            foreach ($this->child_model_gallery_list as $id_child => $model_gallery)
            {
                //echo 'child_gallery_id=' . $this->id . '->' . $id_child . "\n";
                if ($id_child != $exclude_id)
                {
                    $this->all_child_gallery_list_html .= '<li idx="' . $model_gallery->id . '">' . $model_gallery->name;
                    if ($include_id)
                    {
                        $this->all_child_gallery_list_html .= ' (' . $model_gallery->id . ')';
                    }

                    $this->all_child_gallery_list_html .= $model_gallery->getGalleryListHtml($exclude_id, $include_id);
                    $this->all_child_gallery_list_html .= '</li>';

                }

            }
            $this->all_child_gallery_list_html .= '</ul>';
        }

        return $this->all_child_gallery_list_html;
    }

    /**
     * Возвращает опции "Отношение к курению"
     */
    public static function getOptionsParentGallery($add_empty = true)
    {
        $id_current_gallery = \xtetis\xengine\helpers\RequestHelper::get('id_gallery', 'int', 0);

        $model_root_gallery = new \xtetis\ximg\models\GalleryModel(
            [
                'id_parent' => 0,
            ]
        );

        $ret = $model_root_gallery->getGalleryOptions(1, $id_current_gallery);

        if ($add_empty)
        {
            $ret = $ret + [0 => 'В корне'];
        }

        return $ret;
    }

    /**
     * Возвращает массив моделей изображений для текущей галереи
     */
    public function getImgModelList()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        $sql = '
            SELECT id FROM ximg_img WHERE id_gallery = :id_gallery
        ';
        $params = [
            'id_gallery' => $this->id,
        ];
        // Получаем список дочерних моделей
        $this->img_model_list = \xtetis\ximg\models\ImgModel::getModelListBySql(
            $sql, 
            $params,
            [
                'cache'=>true,
                'cache_tags'=>[
                    'ximg_img'
                ]
            ]
        );

        return $this->img_model_list;
    }

    /**
     * Возвращает модель главного изображения галереи или false, если изображение не найдено
     * (пока это первое попавшееся изображение в гареее)
     */
    public function getMainImgModel()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->getImgModelList();

        if (count($this->img_model_list))
        {
            foreach ($this->img_model_list as $id_img => &$model_img)
            {
                return $model_img;
            }
        }

        return false;
    }

    /**
     * Возвращает src гласной картинки альбома
     */
    public function getMainImgSrc()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $model_image = $this->getMainImgModel();

        if ($model_image)
        {
            return $model_image->getImgSrc(true);
        }
        else
        {
            return \xtetis\ximg\models\ImgModel::getNoimageSrc();
        }
    }

    /**
     * Возвращает список моделей для админки
     */
/*
    public static function getSearchModelInfo($params = [])
    {

        if (!isset($params['limit']))
        {
            $params['limit'] = 20;
        }
        $params['limit'] = intval($params['limit']);

        if (!isset($params['offset']))
        {
            $params['offset'] = 0;
        }
        $params['offset'] = intval($params['offset']);

        $where = ['TRUE'];

        if (isset($params['id_category']))
        {
            $id_category       = intval($params['id_category']);
            if ($id_category)
            {
                $where['category'] = 'id_category IN (' . $id_category . ')';
            }
        }

        $sql = '
        SELECT id FROM ximg_gallery WHERE ' . implode(' AND ', $where) . '
        LIMIT ' . $params['limit'] . '
        OFFSET ' . $params['offset'] . '
        ';

        // Получаем список моделей
        $model_list = self::getModelListBySql($sql, []);

        $sql = '
        SELECT COUNT(*) count FROM ximg_gallery WHERE ' . implode(' AND ', $where) . '
        ';

        // Получаем количество
        $count = self::getCountBySql($sql);

        return [
            'model_list' => $model_list,
            'count'      => $count,
        ];
    }
*/
}

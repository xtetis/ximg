<?php

namespace xtetis\ximg\models;

class ImgModel extends \xtetis\xengine\models\TableModel
{
    /**
     * @var string
     */
    public $table_name = 'ximg_img';
    /**
     * ID
     */
    public $id = 0;

    /**
     * ID галереи
     */
    public $id_gallery = 0;

    /**
     * Путь к файлу
     */
    public $path = '';

    /**
     * Размер файла
     */
    public $size = '';

    /**
     * Хэш файла
     */
    public $hash = '';

    // ========================================

    /**
     * Полный путь к файлу изображения
     */
    public $filename = '';

    /**
     * Относительный путь к файлу в директории XIMG_PATH
     */
    public $file_path_ximg = '';

    /**
     * Размер файла в байтах
     */
    public $filesize = 0;

    /**
     * Максимальный размер файла в байтах 2Mb
     */
    public $max_filesize = 2 * 1024 * 1024;

    /**
     * MIME тип файла
     */
    public $mime_content_type = '';

    /**
     * Допустимые MIME типы файла
     */
    public $allow_mime_type_list = [
        'image/jpeg',
        'image/x-citrix-jpeg',
        'image/pjpeg',
        'image/png',
    ];

    /**
     * Расширение файла
     */
    public $extension = '';

    /**
     * Допустимые расширения файла
     */
    public $allow_extension_list = [
        'JPG',
        'JPEG',
        'PNG',

    ];

    /**
     * MD5 хеш файла
     */
    public $file_md5_hash = '';

    /**
     * Результат получения данных из SQL запроса
     */
    public $result_sql = [];

    /**
     * Данные файла для загрузки (в base64)
     */
    public $filedata = '';

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {
        parent::__construct($params);

        // Проверяет параметры
        \xtetis\ximg\Config::validateParams();
    }

    /**
     * Добавляет данные в БД об изображении
     *
     * Обязвтельные параметры
     *      $this->id_gallery
     *      $this->filename
     */
    public function addImgFromFile()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_gallery = intval($this->id_gallery);
        $this->filename   = strval($this->filename);

        $this->filename = realpath($this->filename);

        // Проверяем существование файла
        if (!file_exists($this->filename))
        {
            $this->addError('filename', 'Файл с изображением не найден');

            return false;
        }

        if (!is_file($this->filename))
        {
            $this->addError('filename', 'Не является файлом');

            return false;
        }

        // Проверяем размер файла
        if (!filesize($this->filename))
        {
            $this->addError('filename', 'Размер файла - пустой');

            return false;
        }

        $this->filesize = filesize($this->filename);

        // Проверяем размер файла
        if (!$this->filesize)
        {
            $this->addError('filesize', 'Размер файла - пустой');

            return false;
        }

        if ($this->filesize > $this->max_filesize)
        {
            $this->addError('filesize', 'Максимальный размер обрабатываемого файла ' . $this->max_filesize . ' b');

            return false;
        }

        // Проверяем MIME тип
        $this->mime_content_type = mime_content_type($this->filename);

        if (!$this->mime_content_type)
        {
            $this->addError('mime_content_type', 'Не удалось получить MIME тип файла');

            return false;
        }

        if (!in_array($this->mime_content_type, $this->allow_mime_type_list))
        {
            $this->addError('mime_content_type', 'Недопустимый MIME тип файла');

            return false;
        }

        // проверяем расширение файла
        $path_parts      = pathinfo($this->filename);
        $this->extension = $path_parts['extension'];
        $this->extension = strtoupper($this->extension);

        if (!in_array($this->extension, $this->allow_extension_list))
        {
            $this->addError('mime_content_type', 'Недопустимое расширение файла');

            return false;
        }

        // получаем хэш файла (md5)
        $this->file_md5_hash = md5_file($this->filename);

        if (!$this->file_md5_hash)
        {
            $this->addError('file_md5_hash', 'Не удалось получить md5 хеш файла');

            return false;
        }

        $this->file_path_ximg = str_replace(XIMG_PATH, '', $this->filename);

        $this->hash = $this->file_md5_hash . '_' . uniqid();
        $this->path = $this->file_path_ximg;
        $this->size = $this->filesize;

        return $this->addImg();
    }

    /**
     * Добавляет данные в БД об изображении
     */
    public function addImg()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->id_gallery = intval($this->id_gallery);
        $this->hash       = strval($this->hash);
        $this->path       = strval($this->path);
        $this->size       = intval($this->size);

        if (!$this->id_gallery)
        {
            $this->addError('id_gallery', __FUNCTION__ . ': Не указан параметр id_gallery');

            return false;
        }

        if (!strlen($this->hash))
        {
            $this->addError('hash', 'Не указан параметр hash');

            return false;
        }

        if (!strlen($this->path))
        {
            $this->addError('path', 'Не указан параметр path');

            return false;
        }

        if (!$this->size)
        {
            $this->addError('size', 'Не указан параметр size');

            return false;
        }

        $this->result_sql['addImg'] = \xtetis\ximg\models\SqlModel::addImg(
            $this->id_gallery,
            $this->path,
            $this->size,
            $this->hash
        );

        if ($this->result_sql['addImg']['result'] < 0)
        {
            $this->addError('add_img_result', $this->result_sql['addImg']['result_str']);

            return false;
        }

        return true;
    }

    /**
     * @param $return_noimage_if_not_exists
     */
    public function getImgSrc($return_noimage_if_not_exists = true)
    {
        if ($this->getErrors())
        {
            if ($return_noimage_if_not_exists)
            {
                return self::getNoimageSrc();
            }
            else
            {
                return false;
            }
        }

        return XIMG_SRC . $this->path;
    }

    /**
     * Генерирует и возвращает полный путь к изображению
     */
    public function getImgFullPath($error_if_not_exists = true)
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->filename = XIMG_PATH . $this->path;

        if ($error_if_not_exists)
        {
            if (!file_exists($this->filename))
            {
                $this->addError('common', __FUNCTION__ . ': Не удалось получить полный путь к изображению');

                return false;
            }
        }

        return $this->filename;
    }

    /**
     * Удаляет изображение (файл и запись в БД)
     */
    public function deleteImage()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->getImgFullPath(false);

        @unlink($this->filename);

        if (file_exists($this->filename))
        {
            $this->addError('common', __FUNCTION__ . ': Не удалось удалить файл на сервере');

            return false;
        }

        $this->result_sql['deleteImage'] = \xtetis\ximg\models\SqlModel::deleteImage(
            $this->id
        );

        return true;
    }

    /**
     * Возвращает путь к изобращению NOIMAGE
     */
    public static function getNoimageSrc()
    {
        return XIMG_NOIMAGE_SRC;
    }

    /**
     * Загружает файл и сохраняет еёго в галерее
     * Параметры
     *  - filedata (Данные файла для загрузки (в base64))
     *  - id_gallery - (ID галереи)
     */
    public function uploadImageAndSaveInGallery()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id_gallery = intval($this->id_gallery);
        $this->filedata   = strval($this->filedata);

        // Проверяем существование галереи
        $model_galery = \xtetis\ximg\models\GalleryModel::generateModelById($this->id_gallery);
        if (!$model_galery)
        {
            $this->addError('id_gallery', 'Галерея не существует');

            return false;
        }

        $img_upload_model = new \xtetis\ximg\models\ImgUploadModel([
            'id_gallery' => $this->id_gallery,
            'filedata'   => $this->filedata,
        ]);
        if (!$img_upload_model->uploadFile())
        {
            $this->addError('filedata', $img_upload_model->getLastErrorMessage());

            return false;
        }



        $img_model = new \xtetis\ximg\models\ImgModel([
            'id_gallery' => $this->id_gallery,
            'filename'   => $img_upload_model->upload_full_path,
        ]);
        if (!$img_model->addImgFromFile())
        {
            @unlink($img_upload_model->upload_full_path);
            $this->addError('filedata', $img_model->getLastErrorMessage());

            return false;
        }

        // Пытаемся сделать ресайз изображения

        return true;
    }

    /**
     * Ресайз изображения c сохранением пропорций
     */
    public static function scaleImageMax(
        $filename_img,
        $max_size = 1000
    )
    {
        $model_simple_image = new \xtetis\ximg\models\SimpleImageModel();
        $model_simple_image->load($filename_img);
        if ($model_simple_image->getHeight()>$max_size)
        {
            $model_simple_image->resizeToHeight($max_size);
        }

        if ($model_simple_image->getWidth()>$max_size)
        {
            $model_simple_image->resizeToWidth($max_size);
        }

        $model_simple_image->save($filename_img);
    }
}

<?php

namespace xtetis\ximg\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class SqlModel
{

    /**
     * Добавляем данные об изображении
     */
    public static function addImg(
        $id_gallery = 0,
        $path = '',
        $size = 0,
        $hash = ''
    )
    {

        $id_gallery = intval($id_gallery);
        $size       = intval($size);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL ximg_add_img(:id_gallery,:path,:size,:hash,@result,@result_str)');

        $stmt->bindParam('path', $path, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->bindParam('size', $size, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('hash', $hash, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('id_gallery', $id_gallery, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt = $connect->prepare('SELECT @result as result, @result_str as result_str; ');

        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result']     = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }

    /**
     * Возвращает массив с ID галереи по родительскому элементу
     */
    public static function getGalleryIdByParent(
        $id_parent = 0
    )
    {
        $id_parent = intval($id_parent);
        $connect   = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt      = $connect->prepare('SELECT id FROM ximg_gallery WHERE COALESCE(id_parent,0) = :id_parent');

        $stmt->bindParam('id_parent', $id_parent, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $rows = $stmt->fetchAll();

        if (!is_array($rows))
        {
            $rows = [];
        }

        $ret = [];
        foreach ($rows as $row)
        {
            $ret[] = intval($row['id']);
        }

        return $ret;
    }




    /**
     * Удаляет запись об изображении в БД
     */
    public static function deleteImage(
        $id = 0
    )
    {
        $id = intval($id);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('DELETE FROM ximg_img WHERE id = :id');

        $stmt->bindParam('id', $id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT, 300);
        $stmt->execute();
        $row = $stmt->fetch();

        return $row;
    }

    

}

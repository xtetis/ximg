<?php

namespace xtetis\ximg\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class CategoryModel extends \xtetis\xengine\models\TableModel
{

    /**
     * ID статьи
     */
    public $id = 0;

    /**
     * @var string
     */
    public $table_name = 'ximg_category';

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        /*
        'id_user'          => \xtetis\xuser\models\UserModel::class,
        'id_gallery_cover' => \xtetis\ximg\models\GalleryModel::class,
        'id_gallery_place' => \xtetis\ximg\models\GalleryModel::class,
        */
    ];


    /**
     * Название группы
     */
    public $name = '';

    /**
     * Дата создания записи
     */
    public $create_date = '';

    /**
     * ВОзвращает модели всех категорий
     */
    public static function getAllCategoryModels()
    {
        $sql = 'SELECT id FROM ximg_category';

        return self::getModelListBySql(
            $sql
        );
    }

    /**
     * Добавляет категорию
     */
    public function addCategory()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->name      = strval($this->name);
        $this->name  = strip_tags($this->name);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указан name');

            return false;
        }


        $this->insert_update_field_list = [
            'name',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }

    /**
     * Редактирует категорию
     */
    public function editCategory()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->name      = strval($this->name);
        $this->id        = intval($this->id);
        $this->name = strip_tags($this->name);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указан name');

            return false;
        }

        if (mb_strlen($this->name) > 199)
        {
            $this->addError('name', 'Имя группы не может быть больше 199 символов');

            return false;
        }

        if (!$this->id)
        {
            $this->addError('id', 'Не указан ID группы');

            return false;
        }

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Категория с id=' . $this->id . ' не существует');

            return false;
        }


        $this->insert_update_field_list = [
            'name',
        ];
        if (!$this->updateTableRecordById())
        {
            return false;
        }

        return true;
    }

    /**
     * Удаляет категорию из БД
     */
    public function deleteCategory()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->deleteTableRecordById();

        return true;
    }




    /**
     * Возвращает список категорий опций для select
     */
    public static function getOptions(
        $add_root = true
    )
    {

        $ret = [];

        if ($add_root)
        {
            $ret[0] = 'Все категории';
        }

        foreach (self::getAllCategoryModels() as $id => $model)
        {
            $ret[$id] = $model->name;
        }

        return $ret;
    }

}

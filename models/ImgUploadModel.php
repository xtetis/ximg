<?php

namespace xtetis\ximg\models;

class ImgUploadModel extends \xtetis\xengine\models\Model
{
    /**
     * ID
     */
    public $id = 0;

    /**
     * ID галереи
     */
    public $id_gallery = 0;

    /**
     * Путь к файлу
     */
    public $path = '';

    /**
     * Размер файла
     */
    public $size = '';

    /**
     * Хэш файла
     */
    public $hash = '';

    // ========================================

    /**
     * Полный путь к файлу
     */
    public $filename = '';

    /**
     * Относительный путь к файлу в директории XIMG_PATH
     */
    public $file_path_ximg = '';

    /**
     * Размер файла в байтах
     */
    public $filesize = 0;

    /**
     * Максимальный размер файла в байтах 2Mb
     */
    public $max_filesize = 2 * 1024 * 1024;

    /**
     * MIME тип файла
     */
    public $mime_content_type = '';

    /**
     * Допустимые MIME типы файла
     */
    public $allow_mime_type_list = [
        'image/jpeg'          => 'jpg',
        'image/x-citrix-jpeg' => 'jpg',
        'image/pjpeg'         => 'jpg',
        'image/png'           => 'png',
    ];

    /**
     * Расширение файла
     */
    public $extension = '';

    /**
     * MD5 хеш файла
     */
    public $file_md5_hash = '';

    /**
     * Результат получения данных из SQL запроса
     */
    public $result_sql = [];

    /**
     * Данные файла для загрузки
     */
    public $filedata = false;

    /**
     * Тип данных для загрузки
     */
    public $type = '';

    /**
     * Двоичные данные файла
     */
    public $filedata_binary = false;

    // Временное имя файла
    /**
     * @var string
     */
    public $tmp_filename = '';

    /**
     * временное имя файла
     */
    public $common_input_filename = '';

    /**
     * Полный путь к загружаемому файлу
     */
    public $upload_full_path = '';

    /**
     * Директория загружаемого файла
     */
    public $upload_full_path_directory = '';

    /**
     * @param array $params
     */
    public function __construct($params = [])
    {

        if ($this->getErrors())
        {
            return false;
        }

        $allow_create_params = [
            'filedata',
            'type',
            'id_gallery',
        ];

        foreach ($allow_create_params as $allow_create_params_item)
        {
            if (
                (isset($params[$allow_create_params_item])) &&
                (property_exists($this, $allow_create_params_item))
            )
            {
                $this->$allow_create_params_item = $params[$allow_create_params_item];
            }
        }

        // Проверяет параметры
        \xtetis\ximg\Config::validateParams();

    }

    /**
     * Сохраняет изображение в файл
     * Обязательные параметры
     *      - id_gallery
     *      - type
     *      - filedata
     *
     */
    public function uploadFile()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->type = strval($this->type);
        $this->id_gallery = intval($this->id_gallery);

        if (!$this->checkUploadFile())
        {
            return false;
        }

        if (!$this->id_gallery)
        {
            $this->addError('id_gallery', 'Не указан параметр id_gallery');

            return false;
        }

        if ('base64' == $this->type)
        {
            $this->filedata = strval($this->filedata);

            $this->filedata = preg_replace('/^data:image\/[a-z]+;base64,/', '', $this->filedata);

            if (!strlen($this->filedata))
            {
                $this->addError('filedata', 'Данные для загрузки не указаны');

                return false;
            }

            $this->filedata_binary = base64_decode($this->filedata);

            $this->tmp_filename = tempnam('/tmp', '');

            file_put_contents($this->tmp_filename, $this->filedata_binary);
            chmod($this->tmp_filename, 0777);

            $this->common_input_filename = $this->tmp_filename;

        }

        $this->common_input_filename = realpath($this->common_input_filename);

        if (!file_exists($this->common_input_filename))
        {
            $this->addError('file', 'Файл, указанный в common_input_filename не сууществует');

            return false;
        }

        if (!is_file($this->common_input_filename))
        {
            $this->addError('filename', 'Не является файлом');

            return false;
        }

        $this->filesize = filesize($this->common_input_filename);

        // Проверяем размер файла
        if (!$this->filesize)
        {
            $this->addError('filename', 'Размер файла - пустой');

            return false;
        }

        if ($this->filesize > $this->max_filesize)
        {
            $this->addError('filesize', 'Максимальный размер обрабатываемого файла ' . $this->max_filesize . ' b');

            return false;
        }

        // Проверяем MIME тип
        $this->mime_content_type = mime_content_type($this->common_input_filename);

        if (!$this->mime_content_type)
        {
            $this->addError('mime_content_type', 'Не удалось получить MIME тип файла');

            return false;
        }

        if (!isset($this->allow_mime_type_list[$this->mime_content_type]))
        {
            $this->addError('mime_content_type', 'Недопустимый MIME тип файла ' . $this->mime_content_type);

            return false;
        }

        // Делаем ресайз изобрежения 
        \xtetis\ximg\models\ImgModel::scaleImageMax(
            $this->common_input_filename,
            1000
        );


        // Проверяем MIME тип
        $this->mime_content_type = mime_content_type($this->common_input_filename);

        if (!$this->mime_content_type)
        {
            $this->addError('mime_content_type', 'Не удалось получить MIME тип файла');

            return false;
        }

        if (!isset($this->allow_mime_type_list[$this->mime_content_type]))
        {
            $this->addError('mime_content_type', 'Недопустимый MIME тип файла ' . $this->mime_content_type);

            return false;
        }


        $this->extension = strtolower($this->allow_mime_type_list[$this->mime_content_type]);

        // получаем хэш файла (md5)
        $this->file_md5_hash = md5_file($this->common_input_filename);

        if (!$this->file_md5_hash)
        {
            $this->addError('file_md5_hash', 'Не удалось получить md5 хеш файла');

            return false;
        }

        $this->upload_full_path = XIMG_PATH . '/' . intdiv($this->id_gallery, 1000) .
        '/' . $this->id_gallery . '/' . $this->file_md5_hash .
        '.' . $this->extension;

        if (file_exists($this->upload_full_path))
        {
            $this->addError('upload_path', 'Такой файл уже существует в галерее');

            return false;
        }

        if (!is_writable(XIMG_PATH))
        {
            $this->addError('XIMG_PATH', 'Директория XIMG_PATH не доступна для записи');

            return false;
        }

        $this->upload_full_path_directory = dirname($this->upload_full_path);

        if (!file_exists($this->upload_full_path_directory))
        {
            mkdir($this->upload_full_path_directory, 0777, true);
        }

        if (!file_exists($this->upload_full_path_directory))
        {
            $this->addError('upload_full_path_directory', 'Директория для загрузки не существует');

            return false;
        }

        if (!is_writable($this->upload_full_path_directory))
        {
            $this->addError('upload_full_path_directory', 'Директория для загрузки недоступна для записи');

            return false;
        }

        file_put_contents($this->upload_full_path, @file_get_contents($this->common_input_filename));

        chmod($this->upload_full_path, 0777);

        // Удаляем временный файл
        unlink($this->common_input_filename);

        if (!file_exists($this->upload_full_path))
        {
            $this->addError('upload_path', 'Ошибка при созранение файла');

            return false;
        }

        $this->file_path_ximg = str_replace(XIMG_PATH, XIMG_SRC, $this->upload_full_path);

        $this->hash = $this->file_md5_hash;
        $this->path = $this->file_path_ximg;
        $this->size = $this->filesize;

        return true;
    }

    /**
     * Проверяет возможность загрузки изображения
     * (существование галереи не проверятся)
     * Параметры:
     *  - filedata - данные файла в b64
     *  - type (необязательный)
     */
    public function checkUploadFile()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->type = strval($this->type);

        if (!in_array($this->type, ['base64']))
        {
            $this->type = 'base64';
        }

        if ('base64' == $this->type)
        {

            if (!is_string($this->filedata))
            {
                $this->addError('filedata', 'Данные для загрузки должны быть представлены как base64 значение файла  - строка ('.gettype($this->filedata).')');

                return false;
            }

            $this->filedata = strval($this->filedata);

            $this->filedata = preg_replace('/^data:image\/[a-z]+;base64,/', '', $this->filedata);

            if (!strlen($this->filedata))
            {
                $this->addError('filedata', 'Данные для загрузки не указаны');

                return false;
            }

            $this->filedata_binary = base64_decode($this->filedata);

            if (strlen($this->filedata_binary) > $this->max_filesize)
            {
                $this->addError('filesize', 'Максимальный размер обрабатываемого файла ' . $this->max_filesize . ' b');

                return false;
            }

            $this->tmp_filename = tempnam('/tmp', '');

            file_put_contents($this->tmp_filename, $this->filedata_binary);
            chmod($this->tmp_filename, 0777);

            $this->common_input_filename = $this->tmp_filename;

        }

        $this->common_input_filename = realpath($this->common_input_filename);

        if (!file_exists($this->common_input_filename))
        {
            $this->addError('file', 'Файл, указанный в common_input_filename не сууществует');

            return false;
        }

        if (!is_file($this->common_input_filename))
        {
            $this->addError('filename', 'Не является файлом');

            return false;
        }

        $this->filesize = filesize($this->common_input_filename);

        // Проверяем размер файла
        if (!$this->filesize)
        {
            @unlink($this->common_input_filename);
            $this->addError('filename', 'Размер файла - пустой');

            return false;
        }

        if ($this->filesize > $this->max_filesize)
        {
            @unlink($this->common_input_filename);
            $this->addError('filesize', 'Максимальный размер обрабатываемого файла ' . $this->max_filesize . ' b');

            return false;
        }

        // Проверяем MIME тип
        $this->mime_content_type = mime_content_type($this->common_input_filename);

        if (!$this->mime_content_type)
        {
            @unlink($this->common_input_filename);
            $this->addError('mime_content_type', 'Не удалось получить MIME тип файла');

            return false;
        }

        if (!isset($this->allow_mime_type_list[$this->mime_content_type]))
        {
            @unlink($this->common_input_filename);
            $this->addError('mime_content_type', 'Недопустимый MIME тип файла ' . $this->mime_content_type);

            return false;
        }

        $this->extension = strtolower($this->allow_mime_type_list[$this->mime_content_type]);

        // получаем хэш файла (md5)
        $this->file_md5_hash = md5_file($this->common_input_filename);

        if (!$this->file_md5_hash)
        {
            @unlink($this->common_input_filename);
            $this->addError('file_md5_hash', 'Не удалось получить md5 хеш файла');

            return false;
        }

        if (!is_writable(XIMG_PATH))
        {
            @unlink($this->common_input_filename);
            $this->addError('XIMG_PATH', 'Директория XIMG_PATH не доступна для записи');

            return false;
        }
        @unlink($this->common_input_filename);

        return true;
    }

}

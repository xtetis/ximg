<?php

namespace xtetis\ximg;

class Component extends \xtetis\xengine\models\Component
{

    /**
     * Список действий модуля (они же списки страниц)
     *
     * @var array
     */
    public $module_actions = [
        //================ Формы ==============




    ];

    /**
     * Имя модуля в роутах (прописан в composer.json как autoload/psr-4 )
     *
     * @var string
     */
    public $module_name_in_routes = 'xtetis\ximg';



    /**
     * Список роутов для компонента (не APP)
     * Для APP роуты берутся из /engine/config/params.routes.php 
     * В самих компонентах роуты можно переназначать
     */
    public static function getComponentRoutes()
    {

        $routes = [
            // https://lsfinder.com/forum/theme/item/15
            [
                'url'      => '/^\/' . self::getComponentUrl(). '\/img\/get\/(\w+)$/',
                'function' => function (
                    $params = []
                )
                {
                    $hash  = isset($params[1]) ? $params[1] : '';
                    $hash = strval($hash);

                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = 'img';
                    \xtetis\xengine\App::getApp()->query_name     = 'get';


                    if (strlen($hash))
                    {
                        $_GET['hash'] = $hash;
                    }
                },
            ],

            [
                'url'      => '/^\/(' . self::getComponentUrl(). ')(\/(\w+)?(\/(\w+)?(\/(\d+)?)?)?)?$/',
                'function' => function (
                    $params = []
                )
                {
                    \xtetis\xengine\App::getApp()->component_name = isset($params[0]) ? $params[0] : '';
                    \xtetis\xengine\App::getApp()->action_name    = isset($params[2]) ? $params[2] : '';
                    \xtetis\xengine\App::getApp()->query_name     = isset($params[4]) ? $params[4] : '';

                    $id  = isset($params[6]) ? $params[6] : '';

                    if (strlen($id))
                    {
                        $_GET['id'] = $id;
                    }
                },
            ],
        ];

        return $routes;
    }
}

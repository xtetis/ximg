<?php

namespace xtetis\ximg\controllers;

class XimgController extends \xtetis\xengine\models\Module
{
    /**
     * Возвращает директорию, в которой находятся Views
     */
    public static function getPackageViewsDir()
    {
        return __DIR__ . '/../views/';
    }


    /**
     * Возвращает список галерей
     */
    public static function getGaleryList($params = [])
    {

        $articles_per_page = 10;

        $page = \xtetis\xengine\helpers\RequestHelper::get('p', 'int', 1);
        if ($page < 1)
        {
            $page = 1;
        }

        $params_default = [
            'subcategories'           => 1,
            'subcategories_recursive' => 1,
            'page_url'                => '/',
            'limit'                   => $articles_per_page,
            'offset'                  => (($page - 1) * $articles_per_page),
        ];

        $params_common = $params_default;
        foreach ($params as $key => $value)
        {
            $params_common[$key] = $value;
        }

        $article_list_model = new \xtetis\xarticle\models\ArticleListModel(
            $params_common
        );

        $ret['article_count'] = $article_list_model->getTotalCount();
        $ret['article_list']  = $article_list_model->getArticleList();


        $ret['article_model_list'] = [];


        foreach ($ret['article_list'] as $id_article)
        {

            $article_model = new \xtetis\xarticle\models\ArticleModel(
                [
                    'id' => $id_article,
                ]
            );
            $article_model->getById();


            if ($article_model->getErrors())
            {
                \xtetis\xengine\helpers\LogHelper::customDie($article_model->getLastErrorMessage());
            }
            
            $ret['article_model_list'][$id_article] = $article_model;

        }

        $ret['current_page'] = $page;

        $ret['paginate'] = \xtetis\xengine\helpers\PaginateHelper::getPagination(
            $articles_per_page,
            $page,
            $ret['article_count'],
            $params_common['page_url']
        );

        return $ret;
    }

    /**
     * Возвращает
     */
    public static function getGaleryList()
    {
        \xtetis\xengine\App::getApp()->setParam('layout', 'manager');

        $params = [
            'page_url'         => \xtetis\xcms\Component::getModuleUrl('galleries'),
        ];

        $articles_list_info = \xtetis\xarticle\controllers\ArticleController::getArticleList(
            $params
        );

        return \xtetis\xengine\helpers\RenderHelper::renderFile(
            self::getPackageViewsDir() . 'page/article_list.php',
            [
                'articles_list_info' => $articles_list_info,
            ]
        );
    }


}

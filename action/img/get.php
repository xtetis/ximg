<?php

/**
 * Возвращает изображение
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}


$hash = \xtetis\xengine\helpers\RequestHelper::get('hash', 'str', '');
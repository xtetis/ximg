<?php

namespace xtetis\ximg;

class Config extends \xtetis\xengine\models\Model
{
    /**
     * Проверяет параметры
     */
    public static function validateParams()
    {
        if (!defined('XIMG_PATH'))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Не определена константа XIMG_PATH');
        }

        if (!file_exists(XIMG_PATH))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Директория, указанная в XIMG_PATH не существует');
        }

        if (!is_dir(XIMG_PATH))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Путь, указанный в XIMG_PATH не является директорией');
        }

        if(!is_writable(XIMG_PATH)) 
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Директория, указанная в XIMG_PATH не доступна для записи ');
        } 

        if (!defined('XIMG_SRC'))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Не определена константа XIMG_SRC');
        }

        if (!defined('XIMG_NOIMAGE_SRC'))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Не определена константа XIMG_NOIMAGE');
        }
    }

}
